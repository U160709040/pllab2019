#!/bin/bash/perl

# $ for variable, @ for arrays

use strict;
use warnings;


my $s= "This is a string\n";
print $s;
$s=6;
print $s, "\n";

my @array = ("All",  5,   6.3);

print @array, "\n";
print $array[0], "\n";  # we should use $  because it's a single

my @array2 = ([1, 2, 3], [4, 5, 6], [7,8,9]);

print $array2[2][1], "\n";
print @{$ array2[1]}, "\n"; 


my %grades = ("Ali" => 50, "Mehmet" => 75, "Ayşe"=>  80); # you can use ("Ali", 50, "Mehmet", 75, "Ayşe",  80) BUT it makes problems if there's any null

print %grades, "\n";

print $grades{"Mehmet"}, "\n";



# if you assign a list to a scalar, it prints size. but if you put paranthesis, it will acts like a list. 
# @array = ("one, "two");
# $a = @array; 
# ($a) = @array; 



my @array3 = ("one", "two", "three");
my $a = @array3;
print $a, "\n";
my ($b, $c, $d) = @array3;
print "$b$c$d", "\n";
print '$b$c$d', "\n";















