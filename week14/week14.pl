use strict;
use warnings;

print "type q or quit to exit\n";
print "Enter your input:";
my $input = <>;  #read input
chomp $input;   # escape enter cha. only gets input

my $regex = '^\s*$'; #" match if string $str contains 0 or more white space cha

$regex = '^[A-Z]+$';  # string $str contains all capital  letters ( at least one (+ reason of that))   + yı kullanırken dikkat et. 

$regex = '[A-Z]\d*'; # string $str countains a capital letter followed by 0 or more digits 
# IT WILL ACCEPT such as d3434B1, even it has lower cha, it only looks before the number. 

$regex = '^\d+\.\d+$'; #number $n contains some digits  before and after a decimal point (it means float numbers) $ for finished as digits

$regex = '^(\d{1,3}\.){3}\d{1,3}$';
#$regex = ''; # string contains A and b seperated by any two cha's


#$regex = ''; #string does not contains ATG
 until(($input eq "quit")||($input eq "q")) {
	if($input =~ /$regex/) { print "ACCEPTED\n"; }
	else { print "FAILED\n";}
	print "type q or quit to exit\n";
	print "Enter your input:";
	$input = <>;
	chomp $input;
}
